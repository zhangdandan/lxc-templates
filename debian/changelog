lxc-templates (3.0.4.79.g84b0597-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update lintian override info to new format.
  * Update standards version to 4.6.2, no changes needed.

  [ Mathias Gibbens ]
  * Import new upstream snapshot (Closes: #960169, #998095, #1055869)
    - Drop patch applied upstream
    - Rebase existing patch to apply cleanly
    - Add patch describing how to create containers for archived releases
      (Closes: #1036640)
  * d/control:
    - Add myself to Uploaders
  * Add myself to d/copyright
  * Extend d/lxc-templates.lintian-overrides
  * Add d/salsa-ci.yml and d/u/metadata

 -- Mathias Gibbens <gibmat@debian.org>  Sat, 10 Feb 2024 17:13:00 +0000

lxc-templates (3.0.4.48.g4765da8-2) unstable; urgency=medium

  * Add patch to fix detection of debian-ports architectures

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 21 Aug 2023 16:09:30 -0300

lxc-templates (3.0.4.48.g4765da8-1) unstable; urgency=medium

  * New upstream version 3.0.4.48.g4765da8
  * Refresh patches

 -- Antonio Terceiro <terceiro@debian.org>  Mon, 23 May 2022 19:36:10 -0300

lxc-templates (3.0.4-5) unstable; urgency=medium

  [ Gianfranco Costamagna ]
  * d/control:
    - Add Suggests: qemu-user-static (Closes: #973345)

  [ Pierre-Elliott Bécue ]
  * d/control:
    - Add distro-info to the Recommends of lxc-templates (Closes: #974569)
    - Bump Standards-Version to 4.5.1
  * d/p/0004: fix path variable in some templates to avoid catastrophic rm
    (Closes: #839843)

 -- Pierre-Elliott Bécue <peb@debian.org>  Fri, 12 Mar 2021 11:53:24 +0100

lxc-templates (3.0.4-4) unstable; urgency=medium

  * d/patches/0002: Update lxc.debian template to document alternatives to the
    download temmplate (Closes: #959926)
  * d/control:
    - Add mmdebstrap as a recommend
  * d/README.Debian
    - Add some local documentation to the package to help people not using the
    download template
  * d/patches/0003: Handle properly the security repositories for the future
    releases. (Closes: #970067)
  * d/gbp.conf: added default git-buildpackage config
  * Bump debhelper-compat version to 13

 -- Pierre-Elliott Bécue <peb@debian.org>  Sat, 17 Oct 2020 22:42:34 +0200

lxc-templates (3.0.4-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Drop unnecessary dependency on dh-autoreconf.
  * Rely on pre-initialized dpkg-architecture variables.

 -- Pierre-Elliott Bécue <peb@debian.org>  Sun, 19 Apr 2020 11:59:35 +0200

lxc-templates (3.0.4-2) unstable; urgency=medium

  * d/p/0001: [lxc-debian] Handle languages that are only UTF-8 encoded
    (Closes: #950840)
  * Bump Standards-Version to 4.5.0
  * Set Rules-Requires-Root to no

 -- Pierre-Elliott Bécue <peb@debian.org>  Wed, 15 Apr 2020 17:02:34 +0200

lxc-templates (3.0.4-1) unstable; urgency=medium

  * New upstream release 3.0.4
  * d/control:
    - Bump Standards-Version to 4.4.0
    - Use debhelper-compat instead of debian/compat (and raise level to 12)
  * d/lxc-templates.lintian-overrides: Disable warning for access to dpkg DB

 -- Pierre-Elliott Bécue <peb@debian.org>  Tue, 20 Aug 2019 13:49:53 +0200

lxc-templates (3.0.3-1) unstable; urgency=medium

  * d/control:
    - Recommends bridge-utils
  * New upstream release: 3.0.3
  * Release to unstable

 -- Pierre-Elliott Bécue <peb@debian.org>  Tue, 04 Dec 2018 08:47:01 +0100

lxc-templates (3.0.2-1~exp+1) experimental; urgency=medium

  * New source package as upstream extracted the templates generation
    mechanism from the main lxc project. Hence, no ITP to close.

 -- Pierre-Elliott Bécue <peb@debian.org>  Sat, 17 Nov 2018 21:06:57 +0100
